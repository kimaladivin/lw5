#!/usr/bin/env python2
"""
This node implement RRT* path planning on a known map.

It subscribes to:
    - `/map` (nav_msgs/OccupancyGrid): the known map.
    - `/tf` (tf/tfMessage): location of the robot.
    - `/goal` (geometry_msgs/PoseStamped): the goal location.
    - `/cancel_plan` (std_msgs/Bool): to cancel the current goal.

It publishes:
    - `/plan` (nav_msgs/Path): the constructed path.
    - `/tmp_plan` (nav_msgs/Path): anytime paths.
    - `/inflated_map` (nav_msgs/OccupancyGrid): the inflated obstacle
        map (in which planning is done).
    - '/tree' (visualization_msgs/Marker): the resulting tree.

Parameters:
    - `robot_radius`: radius of the robot for obstacle inflation [m]
        (default: 0.205).
    - `robot_frame`: name of the robot /tf frame (default:
        'base_footprint').
    - `eta`: distance of steer in RRT* algorithm [m] (default: 0.25).
    - `N`: number of samples to generate per iteration in RRT*
        (default: 25).
    - `N_tot`: total number of samples to generate (default: 2500).
        If 0 or less, sampling does not stop.
"""

# Python 2 and 3 compatibility
from __future__ import print_function
# Python standard modules
import math
from threading import RLock
# Python external modules
# ROS modules
import rospy
import tf2_ros
import tf2_geometry_msgs  # pylint: disable=unused-import # noqa: F401
from geometry_msgs.msg import Point, PoseStamped
from nav_msgs.msg import OccupancyGrid, Path
from std_msgs.msg import Bool
from visualization_msgs.msg import Marker
# local modules
from st5_generic import grid_map
from st5_generic.utils import fill_occ_grid_msg, get_param
from st5_generic.geometry import (pose2d_from_pose, pose_from_pose2d,
                                  pose_from_tf)
import rrtstar_class


class RrtStarPlanner(object):
    """Main class implementing RRT* path planning node.

    The `run` method must be called.
    """

    def __init__(self):
        """Create subscriber and publishers."""
        # parameters
        self.robot_frame = get_param('~robot_frame', 'base_footprint')
        self.robot_radius = get_param('~robot_radius', 0.205)
        eta = get_param('~eta', 0.25)
        self.N = get_param('~N', 25)
        self.N_tot = get_param('~N_tot', 2500)
        self.timeout = rospy.Duration(0.5)
        self.occ_threshold = 50
        self.run_rate = 10
        # internal state
        self.obstacle_map = None
        self.map_frame = None
        self.rrtstar = rrtstar_class.RrtStar(eta, self.occ_threshold)
        self.goal_lock = RLock()
        self.goal = None
        self.cur_N = 0
        # create publishers
        self.path_pub = rospy.Publisher('/plan', Path, queue_size=1,
                                        latch=True)
        self.tmp_path_pub = rospy.Publisher('/tmp_plan', Path, queue_size=1,
                                            latch=True)
        self.inflated_map_pub = rospy.Publisher('/inflated_map', OccupancyGrid,
                                                queue_size=1, latch=True)
        self.tree_pub = rospy.Publisher('/tree', Marker, queue_size=1,
                                        latch=True)
        # create subscribers
        self.tf_buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)
        self.map_sub = rospy.Subscriber('/map', OccupancyGrid, self.map_cb,
                                        queue_size=1)
        self.goal_sub = rospy.Subscriber('/goal', PoseStamped, self.goal_cb,
                                         queue_size=1)
        self.cancel_sub = rospy.Subscriber('/cancel_plan', Bool,
                                           self.cancel_cb, queue_size=1)

    def map_cb(self, msg):
        """Create obstacle map from occupancy grid."""
        # extract info from message
        res = msg.info.resolution
        width = msg.info.width
        height = msg.info.height
        origin_x = msg.info.origin.position.x
        origin_y = msg.info.origin.position.y
        data = msg.data
        # create GridMap
        occupancy_grid_map = grid_map.GridMap.from_message(
            res, width, height, origin_x, origin_y, data)
        # inflate obstacles
        self.obstacle_map = occupancy_grid_map.inflate(self.robot_radius)
        # map frame
        self.map_frame = msg.header.frame_id
        # create map message
        map_msg = fill_occ_grid_msg(self.obstacle_map, self.map_frame)
        # publish
        self.inflated_map_pub.publish(map_msg)

    def goal_cb(self, msg):
        """Initialize planning from current robot position to goal."""
        rospy.loginfo('[RRT*] Goal received.')
        if self.obstacle_map is None:
            rospy.logwarn('[RRT*] No map: ignoring goal.')
            return
        # get goal position
        try:
            goal_pose = self.tf_buffer.transform(msg, self.map_frame,
                                                 self.timeout)
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException,
                tf2_ros.ExtrapolationException) as e:
            rospy.logwarn('[RRT*] Could not transform goal into %s [%s]: '
                          'ignoring goal.', self.map_frame,
                          type(e).__name__.split('.')[-1])
            return
        goal_x, goal_y, goal_yaw = pose2d_from_pose(goal_pose.pose)
        # get current robot position
        try:
            robot_tfs = self.tf_buffer.lookup_transform(
                self.map_frame, self.robot_frame, msg.header.stamp,
                self.timeout)
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException,
                tf2_ros.ExtrapolationException) as e:
            rospy.logwarn('[RRT*] Could not get transformation between %s and '
                          '%s [%s]: ignoring goal.', self.map_frame,
                          self.robot_frame, type(e).__name__.split('.')[-1])
            return
        robot_x, robot_y, _ = pose2d_from_pose(pose_from_tf(
            robot_tfs.transform))
        with self.goal_lock:
            self.cur_N = 0
            self.goal = (goal_x, goal_y, goal_yaw)
            # initialize search
            #######################
            # TODO complete below #
            #  |   |   |   |   |  #
            #  v   v   v   v   v  #
            #######################
            # around 1-2 lines
            zoba=(robot_x,robot_y)
            self.rrtstar.initialize_search(zoba, self.goal, self.obstacle_map)
            # publish empty results
            self._pub_path(None, tmp=False)
            if self.N_tot > 0:
                self._pub_path(None, tmp=True)
            self._pub_tree([])

    def cancel_cb(self, msg):
        """Cancel planning by deleting current goal."""
        rospy.loginfo('[RRT*] Canceling current goal.')
        with self.goal_lock:
            self.goal = None

    def run(self):
        """Run search and publish internal state."""
        r = rospy.Rate(self.run_rate)
        while not rospy.is_shutdown():
            r.sleep()
            with self.goal_lock:
                if self.goal is None:
                    continue
                # expand tree
                #######################
                # TODO complete below #
                #  |   |   |   |   |  #
                #  v   v   v   v   v  #
                #######################
                self.rrtstar.expand_tree(self.N)
                # around 1 lines
                self.cur_N += self.N
                # get current path and tree edges
                path = self.rrtstar.current_path
                tree = self.rrtstar.edges
                # publish current state
                tmp = self.cur_N < self.N_tot
                self._pub_path(path, tmp)
                self._pub_tree(tree)
                # stop if done
                if not tmp and self.N_tot > 0:
                    self.goal = None
                    rospy.loginfo('[RRT*] Stopping search')

    def _pub_path(self, position_path, tmp=True):
        """Publish path."""
        if tmp:
            path_pub = self.tmp_path_pub
        else:
            path_pub = self.path_pub
        if position_path is None:
            path_msg = Path()
            path_msg.header.frame_id = self.map_frame
            path_pub.publish(path_msg)
            return
        # get pose from positions
        pose2d_path = []
        for (cur_x, cur_y), (next_x, next_y) in zip(position_path[:-1],
                                                    position_path[1:]):
            yaw = math.atan2(next_y-cur_y, next_x-cur_x)
            pose2d_path.append((cur_x, cur_y, yaw))
        pose2d_path.append(self.goal)
        # build ROS message
        path_msg = Path()
        path_msg.header.frame_id = self.map_frame
        now = rospy.Time.now()
        path_msg.header.stamp = now
        for pose2d in pose2d_path:
            new_ps = PoseStamped()
            new_ps.header.stamp = now
            new_ps.header.frame_id = self.map_frame
            new_ps.pose = pose_from_pose2d(pose2d)
            path_msg.poses.append(new_ps)
        path_pub.publish(path_msg)

    def _pub_tree(self, edges):
        """Publish RRT tree as a Marker."""
        marker_msg = Marker()
        marker_msg.header.frame_id = self.map_frame
        marker_msg.header.stamp = rospy.Time.now()
        marker_msg.type = Marker.LINE_LIST
        marker_msg.pose.orientation.w = 1.
        marker_msg.color.a = 1.
        marker_msg.color.r = 0.1
        marker_msg.color.g = 0.6
        marker_msg.color.b = 0.3
        marker_msg.scale.x = 0.005
        for node, parent in edges:
            if parent is None:
                parent = node
            marker_msg.points.append(Point(node[0], node[1], 0.))
            marker_msg.points.append(Point(parent[0], parent[1], 0.))
        self.tree_pub.publish(marker_msg)


def main():
    """Create ROS node and instantiate class."""
    try:
        rospy.init_node('rrtstar_planner')
        planner_node = RrtStarPlanner()
        planner_node.run()
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    main()
