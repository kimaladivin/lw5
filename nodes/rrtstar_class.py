"""
RRT* search
"""

# Python 2 nd 3 compatibility
from __future__ import print_function
# standard library
import math
from typing import Dict, List, Optional, Set, Tuple
try:
    from typing import Collection  # Python 3.6
except ImportError:
    from typing import Iterable as Collection
# third party
import numpy as np  # type: ignore
# local modules
from st5_generic.grid_map import GridMap


# type for 2d points
Point = Tuple[float, float]


class RrtStar(object):
    """RRT* search

    The search is incremental. You first `initialize_search` for a
    given query before calling `expand_tree`.
    At any time, you can get the `current_path`.

    Alternatively, the `search` method is doing it all.
    """

    def __init__(self, eta, occ_threshold):
        # parameters
        self.eta = eta
        self.occ_threshold = occ_threshold
        # internal state
        self.nodes = set()  # type: Set[Point]
        self.parents = {}  # type: Dict[Point, Optional[Point]]
        self.costs = {}  # type: Dict[Point, float]
        self.gamma = float('inf')
        self.obstacle_map = None  # type: Optional[GridMap]
        self.goal = None
        self._initialized = False

    @property
    def edges(self):
        # type: () -> List[Tuple[Point, Optional[Point]]]
        """Return tree edges."""
        return list(self.parents.items())

    @property
    def r_near(self):
        # type: () -> None
        """Radius of neighbor search."""
        n = len(self.nodes)
        return min(self.eta, math.sqrt(self.gamma/math.pi * math.log(n)/n))

    def _update_gamma(self):
        # type: () -> None
        """Update the gamma parameter in neighbor search."""
        if self.obstacle_map is None:
            self.gamma = np.inf
            return
        data = np.array(self.obstacle_map.data)
        n_unknown = np.count_nonzero(data == self.obstacle_map.default_value)
        n_occ = np.count_nonzero(data >= self.occ_threshold)
        n_free = data.size - n_unknown - n_occ
        vol_free = self.obstacle_map.resolution**2 * n_free
        gamma_L = 6 * vol_free
        self.gamma = gamma_L

    def sample_free(self):
        # type: () -> Point
        """Sample a point in free space."""
        assert self.obstacle_map is not None
        return self.obstacle_map.sample_free(1, self.occ_threshold-1)[0]

    def nearest(self, point):
        # type: (Point) -> Point
        """Get nearest node in the tree.

        Args:
            point: point to look for nearest neighbor.

        Return:
            nearest point in the tree.
        """
        assert self.nodes, 'No nodes.'
        closest = min(self.nodes, key=lambda x: self.dist(x, point))
        return closest

    def steer(self, p1, p2):
        # type: (Point, Point) -> Point
        """Find point at distance eta from p1 in direction to p2.

        Args:
            p1: starting point.
            p2: point to aim for.

        Return:
            point closest to p2 but not farther away than eta from p1.
        """
        assert self.obstacle_map is not None, 'Need map to steer.'
        d = self.dist(p1, p2)
        if d <= self.eta:
            return p2
        x1, y1 = p1
        x2, y2 = p2
        x = x1 + self.eta * (x2-x1) / d
        y = y1 + self.eta * (y2-y1) / d
        return (x, y)

    def coll_free(self, p1, p2):
        # type: (Point, Point) -> bool
        """Check line of sight between points.

        Args:
            p1: first point.
            p2: second point.

        Return:
            True is no collision, False otherwise.
        """
        assert self.obstacle_map is not None, 'Need map to check collision.'
        unknown = self.obstacle_map.default_value
        for cell in self.obstacle_map.get_ray_cells(*(p1+p2)):
            occ = self.obstacle_map.get_cell(*cell)
            if unknown != occ < self.occ_threshold:
                continue
            return False
        return True

    def near(self, point):
        # type: (Point) -> Collection[Point]
        """Find neighborhood of given point.

        The neighborhood size is governed by self.r_near.

        Args:
            point: the point around which looking for points.

        Return:
            all the points in the tree with distance to point less than
                r_near.
        """
        return {p for p in self.nodes if self.dist(p, point) <= self.r_near}

    @staticmethod
    def dist(point1, point2):
        # type: (Point, Point) -> float
        """Euclidean distance between point1 and point2."""
        x1, y1 = point1
        x2, y2 = point2
        return math.hypot(x2-x1, y2-y1)

    def initialize_search(self, start, goal, obstacle_map):
        # type: (Point, Point, GridMap) -> None
        """Initialize new search.

        Args:
            start: starting position.
            goal: goal position.
            obstacle_map: inflated map.
        """
        # internal state
        self.goal = goal
        self.obstacle_map = obstacle_map
        self._update_gamma()
        # initialize search state
        #######################
        # TODO complete below #
        #  |   |   |   |   |  #
        #  v   v   v   v   v  #
        #######################
        self.nodes = {start}
        self.parent_map = {start: None}
        self.cost_map = {start: 0.}
        # around 3 lines
        self._initialized = True

    def expand_tree(self, N=1):
        # type: (int) -> None
        """Add samples to the search tree.

        Args:
            N: number of samples to add (default: 1).
        """
        assert self._initialized, 'Search not initialized.'
        for i in range(N):
            #######################
            # TODO complete below #
            #  |   |   |   |   |  #
            #  v   v   v   v   v  #
            #######################
            x_rand = self.sample_free()
            x_nearest = self.nearest((self.nodes, x_rand))
            x_new = self.steer(x_nearest, x_rand)
            if self.coll_free(x_nearest, x_new):
                X_near = [x for x in self.near(x_new) if self.coll_free(x, x_new)]
                x_min=x_nearest
                for i in X_near :
                    if self.cost_map[i]+self.dist(i,x_new)<self.cost_map[x_min]+self.dist(x_min,x_new):
                        x_min = i
                self.nodes.add(x_new)
                self.parent_map[x_new] = x_min
                self.cost_map[x_new] = self.cost_map[x_min] + self.dist(x_min, x_new)
                for x in X_near:
                    if self.cost_map[x_new] + self.dist(x_new, x) < self.cost_map[x]:
                        self.parent_map[x] = x_new
                        self.cost_map[x] = self.cost_map[x_new] + self.dist(x_new, x)

        # about 20-30 lines

    @property
    def current_path(self):
        # type: () -> Optional[List[Point]]
        """Current path."""
        # abort if goal not found
        #######################
        # TODO complete below #
        #  |   |   |   |   |  #
        #  v   v   v   v   v  #
        #######################
        # about 2 lines
        # build path by backtracking from goal to start
        #######################
        # TODO complete below #
        #  |   |   |   |   |  #
        #  v   v   v   v   v  #
        #######################
        # about 6 lines
        path = [self.goal]
        nearest= self.nearest(self.goal)
        dep=nearest
        while dep is not None :
            path.append(dep)
            dep=self.parent_map[dep]
        path.reverse()
        return path
